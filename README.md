Magento Mobile_Detect
=====================

This extensions adds the Mobile_Detect library as an useable Magento Helper.

## Usage

```
Mage::helper('mobiledetect')->isMobile();
```
