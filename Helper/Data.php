<?php
/**
 * @package SE
 * @category SE_MobileDetect
 *
 * @copyright © 2016 Studio Emma Bvba
 */

/**
 * SE_MobileDetect_Helper_Data.
 *
 * @uses Mobile_Detect
 * @author Studio Emma <info@studioemma.eu>
 */
class SE_MobileDetect_Helper_Data extends Mobile_Detect
{
}
